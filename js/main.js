
$( document ).ready(function() {

    // SVG IE11 support
    svg4everybody();


    $(".btn_modal").fancybox({
        'padding'    : 0
    });

    $('.btn_open').on('click', function(e){
        e.preventDefault();
        $.fancybox.close();
        $('.btn_order_open').trigger('click');

    });
});



// Tabs

(function() {

    $('.eighth__tabs_nav > li > a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.eighth__tabs');

        $(this).closest('.eighth__tabs_nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('.eighth__tabs_item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());

// FAQ

(function() {

    $('.tenth__faq_question').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('li').toggleClass('open');
    });

}());


(function() {

    $('.header__cart_mobile').on('click touchstart', function(e){
        e.preventDefault();
        $('body').toggleClass('open_mobile');
    });

}());

