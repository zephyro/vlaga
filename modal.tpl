

<!-- Mobile Modal -->
<div class="order_mobile">
    <div class="order_mobile__wrap">
        <div class="order_mobile__title">купить комплект пылестоп</div>
        <div class="order_mobile__text">Чтобы купить Пылестоп, оставьте свои контакты. Мы вскоре свяжемся с вами. Или позвоните нам: 8 499 112 36 86</div>
        [[!AjaxForm?
        &snippet=`FormIt`
        &form=`tpl.order.form`
        &hooks=`email`
        &emailSubject=`Сообщение с сайта [[++site_name]]`
        &emailFrom=`[[++from_email]]`
        &emailTo=`[[++contact_email]]`
        &emailTpl=`tpl.form.send`
        &validate= `email:required`
        &validationErrorMessage=`Заполнены не все поля!`
        &successMessage=`Ваше сообщение принято! Мы свяжемся с вами в ближайшее время!`
        ]]
    </div>
</div>

<!-- Order Modal -->

<div class="hide">
    <a href="#order" class="btn_order_open btn_modal"></a>
    <div class="modal" id="order">
        <div class="modal__wrap">
            <div class="modal__container">
                <div class="modal__title">купить комплект пылестоп</div>
                <div class="modal__text">Чтобы купить Пылестоп, оставьте свои контакты. Мы вскоре свяжемся с вами.<br/>Или позвоните нам: 8 499 112 36 86</div>
                <div class="modal__form">
                    [[!AjaxForm?
                    &snippet=`FormIt`
                    &form=`tpl.order.form`
                    &hooks=`email`
                    &emailSubject=`Сообщение с сайта [[++site_name]]`
                    &emailFrom=`[[++from_email]]`
                    &emailTo=`[[++contact_email]]`
                    &emailTpl=`tpl.form.send`
                    &validate= `email:required`
                    &validationErrorMessage=`Заполнены не все поля!`
                    &successMessage=`Ваше сообщение принято! Мы свяжемся с вами в ближайшее время!`
                    ]]
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Thanks Modal -->

<div class="hide">
    <a href="#thanks" class="btn_thanks btn_modal"></a>
    <div class="modal modal_blue" id="thanks">
        <div class="modal__wrap">
            <div class="modal__container">
                <div class="thanks_text">
                    <p>Заявка отправлена!</p>
                    <p>Наш менеджер скоро свяжется с вами.</p>
                    <div class="text-center">
                        <a href="#" class="modal_close"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Зима -->
<div class="hide">
    <div class="modal" id="winter">
        <div class="modal__wrap">
            <div class="modal__container">
                <div class="season">

                    <ul class="season__header">
                        <li>
                            <div class="season__title season_winter">
                                <span>Фильтр</span>
                                <strong>зима</strong>
                            </div>
                        </li>
                        <li>
                            <div class="season__image">
                                <div class="season__image_wrap">
                                    <img src="{$_modx->config.assets_url}template/img/season_01.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </li>
                    </ul>

                    <ul class="season__content">
                        <li>
                            <h3>Описание</h3>
                            <div class="season__text season__text_mobile">
                                {$_modx->resource.fourth_winter_intro}
                            </div>
                        </li>
                        <li>
                            <h3>Принцип действия</h3>
                            <div class="season__text">
                                {$_modx->resource.fourth_winter_text}
                            </div>
                        </li>
                        <li>
                            <h3>Преимущества</h3>
                            <div class="season__text">
                                {$_modx->resource.fourth_winter_advantage}
                            </div>
                        </li>
                    </ul>

                    <div class="season__footer">
                        <a href="#" class="btn btn_open">купить пылестоп</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Весна -->
<div class="hide">
    <div class="modal" id="spring">
        <div class="modal__wrap">
            <div class="modal__container">
                <div class="season">

                    <ul class="season__header">
                        <li>
                            <div class="season__title season_spring">
                                <span>Фильтр</span>
                                <strong>Весна</strong>
                            </div>
                        </li>
                        <li>
                            <div class="season__image">
                                <div class="season__image_wrap">
                                    <img src="{$_modx->config.assets_url}template/img/season_02.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </li>
                    </ul>

                    <ul class="season__content">
                        <li>
                            <h3>Описание</h3>
                            <div class="season__text season__text_mobile">
                                {$_modx->resource.fourth_spring_intro}
                            </div>
                        </li>
                        <li>
                            <h3>Принцип действия</h3>
                            <div class="season__text">
                                {$_modx->resource.fourth_spring_text}
                            </div>
                        </li>
                        <li>
                            <h3>Преимущества</h3>
                            <div class="season__text">
                                $_modx->resource.fourth_spring_advantage}
                            </div>
                        </li>
                    </ul>

                    <div class="season__footer">
                        <a href="#" class="btn btn_open">купить пылестоп</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Лето -->
<div class="hide">
    <div class="modal" id="summer">
        <div class="modal__wrap">
            <div class="modal__container">
                <div class="season">

                    <ul class="season__header">
                        <li>
                            <div class="season__title season_summer">
                                <span>Фильтр</span>
                                <strong>Лето</strong>
                            </div>
                        </li>
                        <li>
                            <div class="season__image">
                                <div class="season__image_wrap">
                                    <img src="{$_modx->config.assets_url}template/img/season_03.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </li>
                    </ul>

                    <ul class="season__content">
                        <li>
                            <h3>Описание</h3>
                            <div class="season__text season__text_mobile">
                                {$_modx->resource.fourth_summer_intro}
                            </div>
                        </li>
                        <li>
                            <h3>Принцип действия</h3>
                            <div class="season__text">
                                {$_modx->resource.fourth_summer_text}
                            </div>
                        </li>
                        <li>
                            <h3>Преимущества</h3>
                            <div class="season__text">
                                {$_modx->resource.fourth_summer_advantage}
                            </div>
                        </li>
                    </ul>

                    <div class="season__footer">
                        <a href="#" class="btn btn_open">купить пылестоп</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Осень -->
<div class="hide">
    <div class="modal" id="autumn">
        <div class="modal__wrap">
            <div class="modal__container">
                <div class="season">

                    <ul class="season__header">
                        <li>
                            <div class="season__title season_autumn">
                                <span>Фильтр</span>
                                <strong>Осень</strong>
                            </div>
                        </li>
                        <li>
                            <div class="season__image">
                                <div class="season__image_wrap">
                                    <img src="{$_modx->config.assets_url}template/img/season_03.jpg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </li>
                    </ul>

                    <ul class="season__content">
                        <li>
                            <h3>Описание</h3>
                            <div class="season__text season__text_mobile">
                                {$_modx->resource.fourth_autumn_intro}
                            </div>
                        </li>
                        <li>
                            <h3>Принцип действия</h3>
                            <div class="season__text">
                                {$_modx->resource.fourth_autumn_text}</div>
                        </li>
                        <li>
                            <h3>Преимущества</h3>
                            <div class="season__text">
                                {$_modx->resource.fourth_autumn_advantage}
                            </div>
                        </li>
                    </ul>

                    <div class="season__footer">
                        <a href="#order" class="btn btn_open">купить пылестоп</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Question Modal -->

<div class="hide">
    <a href="#order" class="btn_order_open btn_modal"></a>
    <div class="modal" id="question">
        <div class="modal__wrap">
            <div class="modal__container">
                <div class="modal__title">Свяжитесь с нами</div>
                <div class="modal__text">И мы ответим на все ваши вопросы.<br/>Или позвоните нам: 8 499 112 36 86</div>
                <div class="modal__form">
                    [[!AjaxForm?
                    &snippet=`FormIt`
                    &form=`tpl.question.form`
                    &hooks=`email`
                    &emailSubject=`Сообщение с сайта [[++site_name]]`
                    &emailFrom=`[[++from_email]]`
                    &emailTo=`[[++contact_email]]`
                    &emailTpl=`tpl.form.send`
                    &validate= `email:required`
                    &validationErrorMessage=`Заполнены не все поля!`
                    &successMessage=`Ваше сообщение принято! Мы свяжемся с вами в ближайшее время!`
                    ]]
                </div>
            </div>
        </div>
    </div>
</div>

