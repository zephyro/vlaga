
<!doctype html>
<html class="no-js" lang="">

    <head>
        {include 'main.head'}
    </head>

    <body>

    <!-- Header -->
    {include 'main.header'}
    <!-- -->
    
        <div class="page" id="fullpage">

            <!-- Section 01 -->
            {include 'section.01'}
            <!-- -->

            <!-- Section 02 -->
            {include 'section.02'}
            <!-- -->

            <!-- Section 03 -->
            {include 'section.03'}
            <!-- -->

            <!-- Section 04 -->
            {include 'section.04'}
            <!-- -->

            <!-- Section 05 -->
            {include 'section.05'}
            <!-- -->

            <!-- Section 07 -->
            {include 'section.07'}
            <!-- -->
    
            <div class="section" id="section6">

                <!-- Section 06 -->
                {include 'section.06'}
                <!-- -->

                <!-- Section 08 -->
                {include 'section.08'}
                <!-- -->

                <!-- Section 09 -->
                {include 'section.09'}
                <!-- -->

                <!-- Section 10 -->
                {include 'section.10'}
                <!-- -->

                <!-- Footer -->
                {include 'main.footer'}
                <!-- -->
                
            </div>
            
        </div>

        <!-- Modal -->
        {include 'main.modal'}
        <!-- -->

        <!-- Privacy -->
        <div class="hide">
            <div class="modal" id="privacy">
                <div class="container">
                    <h1 class="text-center">[[pdoField?&id=`2`&field=`pagetitle`]]</h1>
                    [[pdoField?&id=`2`&field=`content`]]
                </div>
            </div>
        </div>

        <!-- Scripts -->
        {include 'main.scripts'}
        <!-- -->

    </body>
</html>

